/**
 * eBay Buy API
 * API to search and browse items listed in eBay
 *
 * OpenAPI spec version: 1.0.0
 * Contact: kumaresan.manickavelu@gmail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.swagger.client.model;

import io.swagger.client.model.Amount;
import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "")
public class MarketingPrice {
  
  @SerializedName("discountPercentage")
  private String discountPercentage = null;
  @SerializedName("discountAmount")
  private Amount discountAmount = null;
  @SerializedName("originalPrice")
  private Amount originalPrice = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public String getDiscountPercentage() {
    return discountPercentage;
  }
  public void setDiscountPercentage(String discountPercentage) {
    this.discountPercentage = discountPercentage;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Amount getDiscountAmount() {
    return discountAmount;
  }
  public void setDiscountAmount(Amount discountAmount) {
    this.discountAmount = discountAmount;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Amount getOriginalPrice() {
    return originalPrice;
  }
  public void setOriginalPrice(Amount originalPrice) {
    this.originalPrice = originalPrice;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MarketingPrice marketingPrice = (MarketingPrice) o;
    return (this.discountPercentage == null ? marketingPrice.discountPercentage == null : this.discountPercentage.equals(marketingPrice.discountPercentage)) &&
        (this.discountAmount == null ? marketingPrice.discountAmount == null : this.discountAmount.equals(marketingPrice.discountAmount)) &&
        (this.originalPrice == null ? marketingPrice.originalPrice == null : this.originalPrice.equals(marketingPrice.originalPrice));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.discountPercentage == null ? 0: this.discountPercentage.hashCode());
    result = 31 * result + (this.discountAmount == null ? 0: this.discountAmount.hashCode());
    result = 31 * result + (this.originalPrice == null ? 0: this.originalPrice.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class MarketingPrice {\n");
    
    sb.append("  discountPercentage: ").append(discountPercentage).append("\n");
    sb.append("  discountAmount: ").append(discountAmount).append("\n");
    sb.append("  originalPrice: ").append(originalPrice).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
