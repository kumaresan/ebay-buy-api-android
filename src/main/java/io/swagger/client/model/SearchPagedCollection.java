/**
 * eBay Buy API
 * API to search and browse items listed in eBay
 *
 * OpenAPI spec version: 1.0.0
 * Contact: kumaresan.manickavelu@gmail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.swagger.client.model;

import io.swagger.client.model.ErrorDetailV3;
import io.swagger.client.model.ItemSummary;
import java.util.*;
import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "")
public class SearchPagedCollection {
  
  @SerializedName("href")
  private String href = null;
  @SerializedName("itemSummaries")
  private List<ItemSummary> itemSummaries = null;
  @SerializedName("brand")
  private String brand = null;
  @SerializedName("limit")
  private Integer limit = null;
  @SerializedName("next")
  private String next = null;
  @SerializedName("offset")
  private Integer offset = null;
  @SerializedName("prev")
  private String prev = null;
  @SerializedName("total")
  private Integer total = null;
  @SerializedName("warnings")
  private List<ErrorDetailV3> warnings = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public String getHref() {
    return href;
  }
  public void setHref(String href) {
    this.href = href;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public List<ItemSummary> getItemSummaries() {
    return itemSummaries;
  }
  public void setItemSummaries(List<ItemSummary> itemSummaries) {
    this.itemSummaries = itemSummaries;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getBrand() {
    return brand;
  }
  public void setBrand(String brand) {
    this.brand = brand;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Integer getLimit() {
    return limit;
  }
  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getNext() {
    return next;
  }
  public void setNext(String next) {
    this.next = next;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Integer getOffset() {
    return offset;
  }
  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getPrev() {
    return prev;
  }
  public void setPrev(String prev) {
    this.prev = prev;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Integer getTotal() {
    return total;
  }
  public void setTotal(Integer total) {
    this.total = total;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public List<ErrorDetailV3> getWarnings() {
    return warnings;
  }
  public void setWarnings(List<ErrorDetailV3> warnings) {
    this.warnings = warnings;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SearchPagedCollection searchPagedCollection = (SearchPagedCollection) o;
    return (this.href == null ? searchPagedCollection.href == null : this.href.equals(searchPagedCollection.href)) &&
        (this.itemSummaries == null ? searchPagedCollection.itemSummaries == null : this.itemSummaries.equals(searchPagedCollection.itemSummaries)) &&
        (this.brand == null ? searchPagedCollection.brand == null : this.brand.equals(searchPagedCollection.brand)) &&
        (this.limit == null ? searchPagedCollection.limit == null : this.limit.equals(searchPagedCollection.limit)) &&
        (this.next == null ? searchPagedCollection.next == null : this.next.equals(searchPagedCollection.next)) &&
        (this.offset == null ? searchPagedCollection.offset == null : this.offset.equals(searchPagedCollection.offset)) &&
        (this.prev == null ? searchPagedCollection.prev == null : this.prev.equals(searchPagedCollection.prev)) &&
        (this.total == null ? searchPagedCollection.total == null : this.total.equals(searchPagedCollection.total)) &&
        (this.warnings == null ? searchPagedCollection.warnings == null : this.warnings.equals(searchPagedCollection.warnings));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.href == null ? 0: this.href.hashCode());
    result = 31 * result + (this.itemSummaries == null ? 0: this.itemSummaries.hashCode());
    result = 31 * result + (this.brand == null ? 0: this.brand.hashCode());
    result = 31 * result + (this.limit == null ? 0: this.limit.hashCode());
    result = 31 * result + (this.next == null ? 0: this.next.hashCode());
    result = 31 * result + (this.offset == null ? 0: this.offset.hashCode());
    result = 31 * result + (this.prev == null ? 0: this.prev.hashCode());
    result = 31 * result + (this.total == null ? 0: this.total.hashCode());
    result = 31 * result + (this.warnings == null ? 0: this.warnings.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class SearchPagedCollection {\n");
    
    sb.append("  href: ").append(href).append("\n");
    sb.append("  itemSummaries: ").append(itemSummaries).append("\n");
    sb.append("  brand: ").append(brand).append("\n");
    sb.append("  limit: ").append(limit).append("\n");
    sb.append("  next: ").append(next).append("\n");
    sb.append("  offset: ").append(offset).append("\n");
    sb.append("  prev: ").append(prev).append("\n");
    sb.append("  total: ").append(total).append("\n");
    sb.append("  warnings: ").append(warnings).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
