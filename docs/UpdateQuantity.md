
# UpdateQuantity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lineItemId** | **String** |  |  [optional]
**quantity** | **String** |  |  [optional]



