
# PricingSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**adjustment** | [**Adjustment**](Adjustment.md) |  |  [optional]
**deliveryCost** | [**Amount**](Amount.md) |  |  [optional]
**deliveryDiscount** | [**Amount**](Amount.md) |  |  [optional]
**fee** | [**Amount**](Amount.md) |  |  [optional]
**priceDiscount** | [**Amount**](Amount.md) |  |  [optional]
**priceSubtotal** | [**Amount**](Amount.md) |  |  [optional]
**tax** | [**Amount**](Amount.md) |  |  [optional]
**total** | [**Amount**](Amount.md) |  |  [optional]



