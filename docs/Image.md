
# Image

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imageUrl** | **String** |  | 
**height** | **Integer** |  |  [optional]
**width** | **Integer** |  |  [optional]



