# OrderApi

All URIs are relative to *https://api.ebay.com/buy*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGuestCheckoutSession**](OrderApi.md#getGuestCheckoutSession) | **GET** /order/v1/guest_checkout_session/{guest_checkoutsession_id} | get details of Guest Checkout Session
[**getGuestPurchaseOrder**](OrderApi.md#getGuestPurchaseOrder) | **GET** /order/v1/guest_purchase_order/{purchase_order_id} | get details of a purchase order
[**initiateGuestCheckoutSession**](OrderApi.md#initiateGuestCheckoutSession) | **POST** /order/v1/guest_checkout_session/initiate | Initiate Guest Checkout Session
[**placeGuestOrder**](OrderApi.md#placeGuestOrder) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/place_order | Place Guest Order
[**updateGuestLineItemQuantity**](OrderApi.md#updateGuestLineItemQuantity) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_quantity | Update Guest Line Item Quantity
[**updateGuestPaymentInfo**](OrderApi.md#updateGuestPaymentInfo) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_payment_info | Update Guest Payment Info
[**updateGuestShippingAddress**](OrderApi.md#updateGuestShippingAddress) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_shipping_address | Update Guest Shipping Address
[**updateGuestShippingOption**](OrderApi.md#updateGuestShippingOption) | **POST** /order/v1/guest_checkout_session/{guest_checkoutsession_id}/update_shipping_option | Update Guest Shipping Option


<a name="getGuestCheckoutSession"></a>
# **getGuestCheckoutSession**
> CheckoutSessionResponse getGuestCheckoutSession(guestCheckoutsessionId)

get details of Guest Checkout Session

details about the Guest Checkout Session

### Example
```java
// Import classes:
//import io.swagger.client.api.OrderApi;

OrderApi apiInstance = new OrderApi();
String guestCheckoutsessionId = "guestCheckoutsessionId_example"; // String | checkoutsession Id
try {
    CheckoutSessionResponse result = apiInstance.getGuestCheckoutSession(guestCheckoutsessionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderApi#getGuestCheckoutSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guestCheckoutsessionId** | **String**| checkoutsession Id |

### Return type

[**CheckoutSessionResponse**](CheckoutSessionResponse.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getGuestPurchaseOrder"></a>
# **getGuestPurchaseOrder**
> GuestPurchaseOrder getGuestPurchaseOrder(purchaseOrderId)

get details of a purchase order

details about the purchase order

### Example
```java
// Import classes:
//import io.swagger.client.api.OrderApi;

OrderApi apiInstance = new OrderApi();
String purchaseOrderId = "purchaseOrderId_example"; // String | purchase order id
try {
    GuestPurchaseOrder result = apiInstance.getGuestPurchaseOrder(purchaseOrderId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderApi#getGuestPurchaseOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseOrderId** | **String**| purchase order id |

### Return type

[**GuestPurchaseOrder**](GuestPurchaseOrder.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="initiateGuestCheckoutSession"></a>
# **initiateGuestCheckoutSession**
> CheckoutSessionResponse initiateGuestCheckoutSession(createGuestCheckoutSessionRequest)

Initiate Guest Checkout Session

Initiate Guest Checkout Session

### Example
```java
// Import classes:
//import io.swagger.client.api.OrderApi;

OrderApi apiInstance = new OrderApi();
CreateGuestCheckoutSessionRequest createGuestCheckoutSessionRequest = new CreateGuestCheckoutSessionRequest(); // CreateGuestCheckoutSessionRequest | Create Guest Checkout Session Request
try {
    CheckoutSessionResponse result = apiInstance.initiateGuestCheckoutSession(createGuestCheckoutSessionRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderApi#initiateGuestCheckoutSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createGuestCheckoutSessionRequest** | [**CreateGuestCheckoutSessionRequest**](CreateGuestCheckoutSessionRequest.md)| Create Guest Checkout Session Request |

### Return type

[**CheckoutSessionResponse**](CheckoutSessionResponse.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="placeGuestOrder"></a>
# **placeGuestOrder**
> PurchaseOrderSummary placeGuestOrder(guestCheckoutsessionId)

Place Guest Order

Place Guest Order

### Example
```java
// Import classes:
//import io.swagger.client.api.OrderApi;

OrderApi apiInstance = new OrderApi();
String guestCheckoutsessionId = "guestCheckoutsessionId_example"; // String | checkoutsession Id
try {
    PurchaseOrderSummary result = apiInstance.placeGuestOrder(guestCheckoutsessionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderApi#placeGuestOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guestCheckoutsessionId** | **String**| checkoutsession Id |

### Return type

[**PurchaseOrderSummary**](PurchaseOrderSummary.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateGuestLineItemQuantity"></a>
# **updateGuestLineItemQuantity**
> CheckoutSessionResponse updateGuestLineItemQuantity(guestCheckoutsessionId, updateQuantity)

Update Guest Line Item Quantity

Update Guest Line Item Quantity

### Example
```java
// Import classes:
//import io.swagger.client.api.OrderApi;

OrderApi apiInstance = new OrderApi();
String guestCheckoutsessionId = "guestCheckoutsessionId_example"; // String | checkoutsession Id
UpdateQuantity updateQuantity = new UpdateQuantity(); // UpdateQuantity | Update Quantity of Item
try {
    CheckoutSessionResponse result = apiInstance.updateGuestLineItemQuantity(guestCheckoutsessionId, updateQuantity);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderApi#updateGuestLineItemQuantity");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guestCheckoutsessionId** | **String**| checkoutsession Id |
 **updateQuantity** | [**UpdateQuantity**](UpdateQuantity.md)| Update Quantity of Item |

### Return type

[**CheckoutSessionResponse**](CheckoutSessionResponse.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateGuestPaymentInfo"></a>
# **updateGuestPaymentInfo**
> CheckoutSessionResponse updateGuestPaymentInfo(guestCheckoutsessionId, updatePaymentInformation)

Update Guest Payment Info

Update Guest Payment Info

### Example
```java
// Import classes:
//import io.swagger.client.api.OrderApi;

OrderApi apiInstance = new OrderApi();
String guestCheckoutsessionId = "guestCheckoutsessionId_example"; // String | checkoutsession Id
UpdatePaymentInformation updatePaymentInformation = new UpdatePaymentInformation(); // UpdatePaymentInformation | Update Payment Information
try {
    CheckoutSessionResponse result = apiInstance.updateGuestPaymentInfo(guestCheckoutsessionId, updatePaymentInformation);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderApi#updateGuestPaymentInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guestCheckoutsessionId** | **String**| checkoutsession Id |
 **updatePaymentInformation** | [**UpdatePaymentInformation**](UpdatePaymentInformation.md)| Update Payment Information |

### Return type

[**CheckoutSessionResponse**](CheckoutSessionResponse.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateGuestShippingAddress"></a>
# **updateGuestShippingAddress**
> CheckoutSessionResponse updateGuestShippingAddress(guestCheckoutsessionId, shippingAddress)

Update Guest Shipping Address

Update Guest Shipping Address

### Example
```java
// Import classes:
//import io.swagger.client.api.OrderApi;

OrderApi apiInstance = new OrderApi();
String guestCheckoutsessionId = "guestCheckoutsessionId_example"; // String | checkoutsession Id
Address shippingAddress = new Address(); // Address | Shipping Address
try {
    CheckoutSessionResponse result = apiInstance.updateGuestShippingAddress(guestCheckoutsessionId, shippingAddress);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderApi#updateGuestShippingAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guestCheckoutsessionId** | **String**| checkoutsession Id |
 **shippingAddress** | [**Address**](Address.md)| Shipping Address |

### Return type

[**CheckoutSessionResponse**](CheckoutSessionResponse.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateGuestShippingOption"></a>
# **updateGuestShippingOption**
> CheckoutSessionResponse updateGuestShippingOption(guestCheckoutsessionId, updateShippingOption)

Update Guest Shipping Option

Update Guest Shipping Option

### Example
```java
// Import classes:
//import io.swagger.client.api.OrderApi;

OrderApi apiInstance = new OrderApi();
String guestCheckoutsessionId = "guestCheckoutsessionId_example"; // String | checkoutsession Id
UpdateShippingOption updateShippingOption = new UpdateShippingOption(); // UpdateShippingOption | Update Shipping Option
try {
    CheckoutSessionResponse result = apiInstance.updateGuestShippingOption(guestCheckoutsessionId, updateShippingOption);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OrderApi#updateGuestShippingOption");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guestCheckoutsessionId** | **String**| checkoutsession Id |
 **updateShippingOption** | [**UpdateShippingOption**](UpdateShippingOption.md)| Update Shipping Option |

### Return type

[**CheckoutSessionResponse**](CheckoutSessionResponse.md)

### Authorization

[OauthSecurity](../README.md#OauthSecurity)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

