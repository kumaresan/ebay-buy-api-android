
# LineItemInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itemId** | **String** |  |  [optional]
**quantity** | **Integer** |  |  [optional]



