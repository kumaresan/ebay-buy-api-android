
# PaymentMethod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  |  [optional]
**logoImage** | [**Image**](Image.md) |  |  [optional]
**paymentMethodBrands** | [**List&lt;PaymentMethodBrand&gt;**](PaymentMethodBrand.md) |  |  [optional]
**paymentMethodMessages** | [**List&lt;PaymentMethodMessage&gt;**](PaymentMethodMessage.md) |  |  [optional]
**paymentMethodType** | **String** |  |  [optional]



