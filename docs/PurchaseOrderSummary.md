
# PurchaseOrderSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseOrderHref** | **String** |  |  [optional]
**purchaseOrderId** | **String** |  |  [optional]
**purchaseOrderPaymentStatus** | **String** |  |  [optional]
**warnings** | [**List&lt;ErrorDetailV3&gt;**](ErrorDetailV3.md) |  |  [optional]



