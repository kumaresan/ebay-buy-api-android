
# CreditCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountHolderName** | **String** |  |  [optional]
**billingAddress** | [**BillingAddress**](BillingAddress.md) |  |  [optional]
**brand** | **String** |  |  [optional]
**cardNumber** | **String** |  |  [optional]
**cvvNumber** | **String** |  |  [optional]
**expireMonth** | **Integer** |  |  [optional]
**expireYear** | **Integer** |  |  [optional]



