
# ItemFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additionalImageUrls** | **List&lt;String&gt;** |  |  [optional]
**ageGroup** | **String** |  |  [optional]
**brand** | **String** |  |  [optional]
**category** | **String** |  |  [optional]
**categoryId** | **String** |  |  [optional]
**color** | **String** |  |  [optional]
**condition** | **String** |  |  [optional]
**conditionId** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**gtin** | **String** |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**itemEndDate** | **String** |  |  [optional]
**itemId** | **String** |  |  [optional]
**material** | **String** |  |  [optional]
**mpn** | **String** |  |  [optional]
**priceCurrency** | **String** |  |  [optional]
**priceValue** | **String** |  |  [optional]
**quantity** | **Integer** |  |  [optional]
**sellerFeedbackPercentage** | **String** |  |  [optional]
**sellerFeedbackScore** | **Integer** |  |  [optional]
**sellerUsername** | **String** |  |  [optional]
**shipsTo** | **String** |  |  [optional]
**size** | **String** |  |  [optional]
**sizeType** | **String** |  |  [optional]
**title** | **String** |  |  [optional]



