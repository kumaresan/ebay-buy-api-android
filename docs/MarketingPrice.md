
# MarketingPrice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discountPercentage** | **String** |  |  [optional]
**discountAmount** | [**Amount**](Amount.md) |  |  [optional]
**originalPrice** | [**Amount**](Amount.md) |  |  [optional]



