
# ItemDigest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ageGroup** | **String** |  |  [optional]
**availabilityStatusForShipToHome** | **String** |  |  [optional]
**brand** | **String** |  |  [optional]
**color** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**image** | [**Image**](Image.md) |  |  [optional]
**itemHref** | **String** |  |  [optional]
**itemId** | **String** |  |  [optional]
**localizedAspects** | [**List&lt;TypedNameValue&gt;**](TypedNameValue.md) |  |  [optional]
**material** | **String** |  |  [optional]
**pattern** | **String** |  |  [optional]
**price** | [**Amount**](Amount.md) |  |  [optional]
**priceDisplayCondition** | **String** |  |  [optional]
**quantitySold** | **Integer** |  |  [optional]
**size** | **String** |  |  [optional]
**sizeSystem** | **String** |  |  [optional]
**sizeType** | **String** |  |  [optional]
**title** | **String** |  |  [optional]



