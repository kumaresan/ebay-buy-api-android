
# ShippingOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shippingCarrierName** | **String** |  |  [optional]
**shippingCost** | [**Amount**](Amount.md) |  |  [optional]
**shippingServiceName** | **String** |  |  [optional]
**trademarkSymbol** | **String** |  |  [optional]
**type** | **String** |  |  [optional]



