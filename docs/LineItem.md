
# LineItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image** | [**Image**](Image.md) |  |  [optional]
**itemId** | **String** |  |  [optional]
**lineItemId** | **String** |  |  [optional]
**lineItemPaymentStatus** | **String** |  |  [optional]
**lineItemStatus** | **String** |  |  [optional]
**netPrice** | [**Amount**](Amount.md) |  |  [optional]
**quantity** | **Integer** |  |  [optional]
**seller** | [**Seller**](Seller.md) |  |  [optional]
**shippingDetail** | [**ShippingDetail**](ShippingDetail.md) |  |  [optional]
**title** | **String** |  |  [optional]



