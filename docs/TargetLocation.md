
# TargetLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unitOfMeasure** | **String** |  |  [optional]
**value** | **String** |  |  [optional]



