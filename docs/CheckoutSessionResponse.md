
# CheckoutSessionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acceptedPaymentMethods** | [**List&lt;PaymentMethod&gt;**](PaymentMethod.md) |  |  [optional]
**checkoutSessionId** | **String** |  |  [optional]
**expirationDate** | **String** |  |  [optional]
**lineItems** | [**List&lt;LineItem&gt;**](LineItem.md) |  |  [optional]
**pricingSummary** | [**PricingSummary**](PricingSummary.md) |  |  [optional]
**providedPaymentInstrument** | [**ProvidedPaymentInstrument**](ProvidedPaymentInstrument.md) |  |  [optional]
**shippingAddress** | [**Address**](Address.md) |  |  [optional]
**warnings** | [**List&lt;ErrorDetailV3&gt;**](ErrorDetailV3.md) |  |  [optional]



