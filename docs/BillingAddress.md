
# BillingAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addressLine1** | **String** |  |  [optional]
**addressLine2** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**county** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**postalCode** | **String** |  |  [optional]
**stateOrProvince** | **String** |  |  [optional]



