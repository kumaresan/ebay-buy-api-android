
# ProvidedPaymentInstrument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentInstrumentReference** | [**PaymentInstrumentReference**](PaymentInstrumentReference.md) |  |  [optional]
**paymentMethodBrand** | [**PaymentMethodBrand**](PaymentMethodBrand.md) |  |  [optional]
**paymentMethodType** | **String** |  |  [optional]



