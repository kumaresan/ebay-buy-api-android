
# ItemGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additionalImages** | [**List&lt;Image&gt;**](Image.md) |  |  [optional]
**categoryPath** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**image** | [**Image**](Image.md) |  |  [optional]
**itemGroupId** | **String** |  |  [optional]
**items** | [**List&lt;ItemDigest&gt;**](ItemDigest.md) |  |  [optional]
**reviewRating** | [**ReviewRating**](ReviewRating.md) |  |  [optional]
**shortDescription** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**variesByLocalizedAspects** | [**List&lt;TypedNameValue&gt;**](TypedNameValue.md) |  |  [optional]
**warnings** | [**List&lt;ErrorDetailV3&gt;**](ErrorDetailV3.md) |  |  [optional]



