
# ErrorDetailV3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  |  [optional]
**domain** | **String** |  |  [optional]
**errorId** | **Integer** |  |  [optional]
**inputRefIds** | **List&lt;String&gt;** |  |  [optional]
**longMessage** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**outputRefIds** | **List&lt;String&gt;** |  |  [optional]
**parameters** | [**List&lt;ErrorParameterV3&gt;**](ErrorParameterV3.md) |  |  [optional]
**subdomain** | **String** |  |  [optional]



