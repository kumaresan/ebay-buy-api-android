
# GuestPurchaseOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lineItems** | [**List&lt;LineItem&gt;**](LineItem.md) |  |  [optional]
**pricingSummary** | [**PricingSummary**](PricingSummary.md) |  |  [optional]
**purchaseOrderCreationDate** | **String** |  |  [optional]
**purchaseOrderId** | **String** |  |  [optional]
**purchaseOrderPaymentStatus** | **String** |  |  [optional]
**purchaseOrderStatus** | **String** |  |  [optional]
**refundedAmount** | [**Amount**](Amount.md) |  |  [optional]
**warnings** | [**List&lt;ErrorDetailV3&gt;**](ErrorDetailV3.md) |  |  [optional]



